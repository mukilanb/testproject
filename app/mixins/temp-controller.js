import Mixin from '@ember/object/mixin';
import { computed, set, observer } from '@ember/object';

export default Mixin.create({
    queryParams: ['place'],
    contact: Ember.inject.service(),
    place: Ember.computed.alias("contact.place"),
});
