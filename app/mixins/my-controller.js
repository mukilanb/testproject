import Mixin from '@ember/object/mixin';
import { computed, set, observer } from '@ember/object';
export default Mixin.create({
    queryParams: ['name'],
    
    contact: Ember.inject.service(),
    name:Ember.computed.alias("contact.name"),
    
});
