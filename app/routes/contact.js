import Route from '@ember/routing/route';
import MyRoute from './../mixins/my-route';
import TempRoute from './../mixins/temp-route';

export default Route.extend(MyRoute,TempRoute, {
    contact:Ember.inject.service(),
    model(params){
        this.get("contact").setName( params.name );
        return {};
    },
});
