import Component from '@ember/component';
import Ember from 'ember';

export default Component.extend({
    contact: Ember.inject.service(),
    name:'',
    place:'',
    id:'',
    actions: {
        addUserValue(){
            if(this.name !== '' || this.place !== ''){
                this.get("contact").setName(this.name);
                this.get("contact").setPlace(this.place);
            }
        },
    }
});
