import Service from '@ember/service';

export default Service.extend({
    name:'',
    place:'',
    setName(name){
        this.set("name", name);
    },
    setPlace(place){
        this.set("place", place);
    },
});
