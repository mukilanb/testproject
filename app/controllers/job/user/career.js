import Controller from '@ember/controller';
import MyController from './../../../mixins/my-controller';
import TempController from './../../../mixins/temp-controller';

export default Controller.extend(MyController,TempController, {
    router: Ember.inject.service(),
    actions: {
        setName(value) {
            this._super(...arguments);
        },
        setPlace(value){
            this._super(...arguments);
        },
        changeRoute(url, event){
            this.get("router").transitionTo(url);
            // event.stopPropagation();
        }
    }
});
