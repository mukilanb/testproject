import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
  didTransition(transition) {
    this._super(...arguments);
    console.log("Did-Transition");
  }
});

Router.map(function() {
  this.route('about', {path: '/' });
  this.route('contact', { path: '/contact/:uid'});

  this.route('job', function() {
    this.route('user', function() {
      this.route('career');
    });
  });
  this.route('support');
});

export default Router;
