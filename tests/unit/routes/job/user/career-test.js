import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | job/user/career', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:job/user/career');
    assert.ok(route);
  });
});
