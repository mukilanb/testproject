import EmberObject from '@ember/object';
import MyRouteMixin from 'super-rentals/mixins/my-route';
import { module, test } from 'qunit';

module('Unit | Mixin | my-route', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let MyRouteObject = EmberObject.extend(MyRouteMixin);
    let subject = MyRouteObject.create();
    assert.ok(subject);
  });
});
