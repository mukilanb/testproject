import EmberObject from '@ember/object';
import MyControllerMixin from 'super-rentals/mixins/my-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | my-controller', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let MyControllerObject = EmberObject.extend(MyControllerMixin);
    let subject = MyControllerObject.create();
    assert.ok(subject);
  });
});
