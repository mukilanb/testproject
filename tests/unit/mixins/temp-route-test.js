import EmberObject from '@ember/object';
import TempRouteMixin from 'super-rentals/mixins/temp-route';
import { module, test } from 'qunit';

module('Unit | Mixin | temp-route', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let TempRouteObject = EmberObject.extend(TempRouteMixin);
    let subject = TempRouteObject.create();
    assert.ok(subject);
  });
});
