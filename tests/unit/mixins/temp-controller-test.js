import EmberObject from '@ember/object';
import TempControllerMixin from 'super-rentals/mixins/temp-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | temp-controller', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let TempControllerObject = EmberObject.extend(TempControllerMixin);
    let subject = TempControllerObject.create();
    assert.ok(subject);
  });
});
